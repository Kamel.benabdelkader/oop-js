export class Person {

    name; 
    firstName;  
    age;

    constructor(paramName,paramFirstName,paramAge){

        this.name = paramName;

        this.firstName = paramFirstName;

        this.age = paramAge;
    }

     greeting() {
        return  "Hello, my name is "+this.name+" " + this.firstName + " , I am "+this.age+ " years old"
        
    }


    toHTML() {
        const createdArticle = document.createElement('article');
        createdArticle.textContent = this.name + ' '+ this.firstName ;
        createdArticle.addEventListener('click', () => {
            alert( this.greeting())
          });
        return createdArticle


    }
    
}
