/*let target = document.querySelector('body')

import {Person} from './Person';

// instance 1
let instance1 = new Person ('kamel', 'BEN', 38)
console.log(instance1);
target.appendChild(instance1.toHTML())



// instance 2
let instance2 = new Person ('Jean', 'DEMEL', 35)



// instance 3
let instance3 = new Person ('Mister', 'BLOUP', 58)
*/
import {Counter} from './Counter';


// counter 1
let counter1 = new Counter()

// counter 2
let counter2 = new Counter ()

counter1.increment();
counter2.decrement();
counter2.reset();


console.log(counter1.value,counter2.value);



